import os
import glob
from datetime import datetime, timedelta
from ics import Calendar, Event
import pytz



def make_calendar(event_name: str, date_string: str, location: str, fpath: str):
    """
    :param date_string:   str like "2022-06-06T12:05:23" (without timzone information)
    """

    fname = os.path.split(fpath)[-1]

    assert fname.endswith(".md")
    fname_cal = f"{fname[:-2]}ics"
    fpath_cal = os.path.join("content", "res", "cal", fname_cal)

    c = Calendar()
    e = Event()
    tz = pytz.timezone('Europe/Berlin')
    e.name = event_name
    e.location = location
    e.begin = tz.localize(datetime.fromisoformat(date_string))
    e.end = tz.localize(datetime.fromisoformat(date_string)) + timedelta(hours=1)
    c.events.add(e)
    with open(fpath_cal, 'w') as my_file:
        my_file.writelines(c.serialize_iter())
        print(f"File written: {fpath_cal}")


def process_pages():

    fpaths = glob.glob("content/pages/*.md")
    fpaths += glob.glob("content/pages/**/*.md")
    fpaths.sort()

    for fpath in fpaths:
        process_md_file(fpath)

def process_md_file(fpath: str):

    with open(fpath, "r") as fp:
        txt = fp.read()

    header_txt = txt.split("\n\n")[0]
    header = get_header_data(header_txt)
    # print(header)

    if cal_date := header.get("cal_date"):
        event_name = header.get("Title", "unknown title")
        location = header.get("cal_location", "unknown location")
        description = header.get("cal_description", "")
        make_calendar(event_name=event_name, date_string=cal_date, location=location, fpath=fpath)

def get_header_data(txt):

    lines = txt.split("\n")
    data = {}
    for line in lines:
        if line.strip().startswith("#"):
            continue
        try:
            idx = line.index(":")
        except ValueError:
            continue
        key = line[:idx].strip()
        value = line[idx+1:].strip()
        data[key] = value

    return data

def main():
    process_pages()

if __name__ == "__main__":
    main()
