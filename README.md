Das ist noch keine umfassende Doku.


## lokale Vorschau

- `pelican -lr`

## Deployment:

- Auf Repo <https://codeberg.org/kddk/website> pushen
- Deployment Script erledigt den Rest


## Kalender-Dateien erzeugen:

- in Markdown-Header `cal_date: 2024-02-05T19:00:00` (mit passenden Werten) eintragen
- Zeitzone wird automatisch auf `Europe/Berlin` gesetzt
- `python make_calendar.py` aufrufen -> erzeugt .ics dateien für alle pages. Ergebnisse landen in `content/res/cal/`
- ics-Datein in markdown verlinken z.B. mit `Zeit: 05.02.2024, 19:00 Uhr - 20:00 Uhr ([.ics-Datei](../res/cal/2024-02-05.ics))`


