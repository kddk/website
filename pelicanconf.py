from datetime import datetime
import time
AUTHOR = 'kddk'
SITENAME = 'KDDK - Initiative für konstruktive digitale Diskussionskultur'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Rome'

DEFAULT_LANG = 'de'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('Python.org', 'https://www.python.org/'),
         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True


THEME = './themes/kddk-theme1'

MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.codehilite': {'css_class': 'highlight'},
        'markdown.extensions.extra': {},
        'markdown.extensions.admonition': {},
        'markdown.extensions.meta': {},
    },
    'output_format': 'html5',
}


STATIC_PATHS = ["img", "res/htaccess", "adm", "res/cal", "files"]

EXTRA_PATH_METADATA = {
                        'res/htaccess': {'path': '.htaccess'},
                      }

ARTICLE_EXCLUDES = ["adm"]
PAGE_EXCLUDES = ["adm"]

# PLUGIN_PATHS = ['pelican-plugins', '.']
# PLUGINS = ['filetime_from_git', 'pelican-open_graph', 'neighbors']
# PLUGINS = ['pelican-open_graph', 'neighbors']

INDEX_SAVE_AS = 'blog.html'

YEAR_ARCHIVE_SAVE_AS = '{date:%Y}.html'
ARTICLE_SAVE_AS = '{date:%Y}/{date:%m}/{slug}.html'
ARTICLE_URL = '{date:%Y}/{date:%m}/{slug}.html'

YEAR_START = 2023
YEAR_END = datetime.utcnow().year

#
BUILD_TIME = time.strftime("%Y-%m-%d %H:%M:%S %z")

# IS_STAGING = bool(os.environ.get("STAGING", ""))
# COMMIT_ID = os.environ.get("COMMIT", "")[:7]
