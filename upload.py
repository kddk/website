import os
import time


try:
    # this will be part of standard library for python >= 3.11
    import tomllib
except ModuleNotFoundError:
    import tomli as tomllib

import ipydex

confpath = "config.toml"


with open(confpath, "rb") as fp:
    CONF = tomllib.load(fp)

user = CONF["user"]
host = CONF["host"]

# example host: foobar.uberspace.de
rsync_cmd = f"rsync -avz --progress  ./output/ {user}@{host}:/home/{user}/html/"

    
os.system(rsync_cmd)
