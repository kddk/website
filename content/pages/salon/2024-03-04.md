Title: 4. März 2024: Salon digitale Diskussionskultur
Date: 2024-02-29 09:00
Author: kddk
save_as: salon/2024-03-04.html
Template: page
og_description: »Gemeinsam sind wir schlau: Was Mündigkeit mit Teamwork zu tun hat«
cal_date: 2024-03-04T19:00:00
cal_location: https://bbb.tu-dresden.de/rooms/4r2-mdq-esj-axj/join


<a href="../">Zurück zur kddk-Hauptseite &nbsp; <img alt="Zurück zur Hauptseite" src="../theme/img/kddk_logo1.png" height="30"></a> /
<a href=".">Veranstaltungsliste</a>

# Salon digitale Diskussionskultur

## 4. März 2024: "Gemeinsam sind wir schlau: Was Mündigkeit mit Teamwork zu tun hat"

- Impulsgeber:in: Paul Reichert
- Zeit: 03.04.2024, 19:00 Uhr - 20:00 Uhr ([.ics-Datei](../res/cal/2024-03-04.ics))
- Teilnahme-Link: (liegt in der Vergangenheit)
- [Folien](../files/salon-2024-03-04.pdf)


---

**Zusammenfassung:**

In diesem Vortrag geht es um eine Idee, dass Bürger:innen in Gruppen mehr Zeit, Geld, Gehirnschmalz und Einfluss haben, um sich eine Meinung zu bilden und damit die gesellschaftliche Selbstwirksamkeit wiederherzustellen.


**Inhalt**:

Das Internet ist eine unerschöpfliche Quelle an Informationen, aus denen die Menschen schöpfen.
Aufgrund der Vielfalt dieser Informationen ist es Massenmedien und anderen Instituationen kaum noch möglich, sich mit den Inhalten dieser Quellen fundiert auseinanderzusetzen.
Deshalb bleiben viele Menschen mit dem Gefühl zurück, nicht gehört zu werden und machtlos zu sein, was auch stimmt.

Doch ich glaube an einen Weg aus der Misere: Gruppen von Bürger:innen, die gemeinsam aktiv nach für sie relevanten Informationen suchen. Mit mehr Zeit, Geld, Gehirnschmalz und Einfluss ausgestattet kann es ihnen gelingen, die gesellschaftliche Selbstwirksamkeit wiederherzustellen.

Der Impulsvortrag (ca. 30min) erklärt den generellen Ansatz (der noch unerprobt ist) und stellt erste Ideen vor, wie solche Gruppen organisiert werden können. Anschließend folgt eine Diskussion mit dem Publikum.


