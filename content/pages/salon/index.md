Title: Salon digitale Diskussionskultur
Date: 2024-01-29 22:00
Author: kddk
save_as: salon/index.html
Template: page
og_description: Übersicht über geplante und vergangene Salon-Veranstaltungen
image: /img/salon-logo1.png


<!--  TODO: das sollte ins template migriert werden (ggf. spezielles Template anlegen) -->
<a href="../">Zurück zur kddk-Hauptseite &nbsp; <img alt="Zurück zur Hauptseite" src="../theme/img/kddk_logo1.png" height="30"></a>

 <div class="h1-conainer">
<h1>Salon digitale Diskussionskultur</h1>
</div>


Im Rahmen dieser Reihe organisieren wir regelmäßig Onlineveranstaltungen, zu denen alle Interessierten beitragen können.


**Kommende Termine:**

- 06\. Mai 2024, 19:00 Uhr: ***Werkstatt digitale Diskussionskultur***: ([mehr Info](./2024-05-06.html))
    - **Zusammenfassung:**
    Wir verlassen den Salon und gehen in die Werkstatt, um die vorgestellten Projekte anzupacken.

**Bisherige Termine:**

- 14\. April 2024, 14:00 Uhr: **Innovative Tools for Debate Online: Insights from vTaiwan** ([mehr Info](./2024-04-14.html))
    - Summary: In this talk g0v and vTaiwan member Cui Jia Wei explores how civic technology and new platforms can foster more informed and inclusive political discussion.
- 04\. März 2024, 19:00 Uhr: ***Gemeinsam sind wir schlau: Was Mündigkeit mit Teamwork zu tun hat*** ([mehr Info](./2024-03-04.html))
    - **Zusammenfassung:**
    In diesem Vortrag geht es um eine Idee, dass Bürger:innen in Gruppen mehr Zeit, Geld, Gehirnschmalz und Einfluss haben, um sich eine Meinung zu bilden und damit die gesellschaftliche Selbstwirksamkeit wiederherzustellen.
- 05\. Februar 2024, 19:00 Uhr: ***Alles muss man selber machen: Ein Vorschlag zur konstruktiven digitalen Diskussionskultur*** ([mehr Info](./2024-02-05.html))
    - **Zusammenfassung:** <br>
    Die Diskussionskultur im Netz ist oft miserabel – wenigstens darüber sind sich alle einig. Der Vortrag stellt 5 Schritte vor, um die Situation zu verbessern: 1. Regeln festlegen, 2. Koalition der Willigen formen, 3. Anreizstrukturen etablieren, 4. Hilfsmittel implementieren, 5. wachsen und lernen.

---

**Zielgruppe Publikum:** Menschen, die die Diskussionskultur grundsätzlich verbessern wollen und dazu Anregungen und Austauschmöglichkeiten suchen.

**Zielgruppe Impulsgeber:innen:** Menschen, die

- ihre Sichtweise oder Expertise beisteuern wollen,
- eine hilfreiche Methode oder Software vorstellen möchten oder
- eine Projektidee haben und Unterstützung suchen.

Wir kündigen die Veranstaltungen über unsere Kanäle an und sorgen für die Moderation.

**Interesse oder Fragen?** [Melde dich bei uns!](../#Kontakt)
