Title: Projekt: Selbstverpflichtung zur konstruktiven Diskussion
Author: kddk
save_as: selbstverpflichtung/index.html
Template: page

<!--  TODO: das sollte ins template migriert werden (ggf. spezielles Template anlegen) -->
<a href="../">Zurück zur kddk-Hauptseite &nbsp; <img alt="Zurück zur Hauptseite" src="../theme/img/kddk_logo1.png" height="30"></a>

<div class="h1-container">
	<h1>Projekt: Selbstverpflichtung zur konstruktiven Diskussion</h1>
</div>

**Motivation und Grundidee**

Auch unter Menschen, die grundsätzlich ähnliche Ziele haben, verlaufen Diskussionen im digitalen Raum oft unkonstruktiv oder sie eskalieren sogar. Die fruchtbaren Beiträge gehen im Rauschen unter. Das kostet Ressourcen, die eigentlich dringend für die konstruktive Arbeit gebraucht werden.

Rauschen und unkonstruktive Beiträge werden sich kaum vollständig abstellen lassen. Ziel dieses Projekts ist es stattdessen, auf Microblogging-Netzwerken eine bessere Trennung zwischen gehaltvollen und unkonstruktiven Beiträgen, zwischen Signal und Rauschen, zu erreichen.

Zentrales Mittel ist der Hashtag **#svkd** (Selbstverpflichtung konstruktive Diskussionskultur). Mit ihm lässt sich signalisieren, dass ein Beitrag den Anspruch hat, ein konstruktiver Diskussionsbeitrag zu sein. Dadurch entsteht die Möglichkeit des Opt-ins in eine niveauvolle Diskussion, einerseits für Autor:innen indem sie Beiträge taggen, und andererseits für Leser:innen indem sie Beiträge ohne #svkd-Tag ausblenden.

**Wozu genau verpflichtet man sich mit dem Hashtag?**

Man verpflichtet sich dazu, einige Grundregeln für die Diskussion einzuhalten. Ein aktueller Entwurf kann unter [https://simplefeedback.kddk.uber.space/doc/selbstverpflichtung-entwurf-1/63d4c](https://simplefeedback.kddk.uber.space/doc/selbstverpflichtung-entwurf-1/63d4c) eingesehen und kommentiert werden. Der Versionsverlauf wird in einem [öffentlichen Git-Repo](https://codeberg.org/kddk/organized-self-commitment) verwaltet.

**Was habe ich davon, mitzumachen?**

* Als Leser:in: **besseren Content**: Du kannst Beiträge ausblenden, die nicht einmal den Anspruch haben, konstruktiv zu sein. Hältst du einen der getaggten Beiträge für fehlgeleitet, dann kannst du damit rechnen, dass der Fehler berichtigt wird.
* Als Autor:in: **mehr Sichtbarkeit**. Deine getaggten Beiträge werden ernster genommen, sowohl von Gleichgesinnten als auch von Kritiker:innen. Wenn einer deiner Beiträge versehentlich unkonstruktiv auf andere wirkt, trägt dein erklärter Anspruch an seine Qualität dazu bei, dass du nicht ignoriert, sondern auf das Missverständnis hingewiesen wirst -- und damit die Möglichkeit hast, die Situation aufzuklären.

**Was kann ich tun?**

* bereitstehen, um mitzumachen
* den [aktuellen Entwurf](https://simplefeedback.kddk.uber.space/doc/selbstverpflichtung-entwurf-1/63d4c) kommentieren
* allgemeines Feedback geben (z.B. besseren Hashtag vorschlagen), siehe Abschnitt [Kontakt](/#Kontakt)
* potentiell interessierte Personen über das Projekt informieren
* Kontakt aufnehmen, um Teil des Orgateams zu werden

**Wo gibt es mehr Infos Idee?**

Die Idee wurde im [ersten kddk-Salon-Vortrag](https://kddk.eu/salon/2024-02-05.html) und auf den [Datenspuren 2024](/ds24) vorgestellt (Folien und Video verfügbar).


**Was ist der aktuelle Stand des Projekts?**

Bald beginnt eine erste Testphase. Es gibt noch einige Baustellen, die derzeit noch bewusst ausgeklammert wurden:

* Umgang mit Trollen: Natürlich hält nichts Trolle davon ab, ebenso den Hashtag zu benutzen. Eine Lösungsidee besteht darin, dass es einen Account gibt, der allen am Projekt teilnehmenden Usern folgt. Wenn deutlich wird, dass jemand aktiv gegen die Regeln verstößt, könnte der Account die Person entfolgen. In der Auflistung der konstruktiven Beiträge
* Tooling: Aktuell ist es bei Mastodon nicht möglich, die Antworten auf einen Beitrag anhand eines Hashtags zu filtern. Hierfür braucht es ein externes Tool, das noch in Entwicklung ist. Funktionsfähiger Prototyp: <https://spov.kddk.eu/> (Repo: <https://codeberg.org/kddk/spov>).

