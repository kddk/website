Title: KDDK: Konstruktive digitale Diskussionskultur
Date: 2023-11-21 20:39:19
Author: kddk
save_as: index.html
Template: page
og_description: Inititative zur Förderung einer konstruktiven digitale Diskussionskultur (Startseite)


<div class="h1-conainer">
<h1>KDDK &nbsp;<img src="theme/img/kddk_logo1.png" width="20%"></h1>
</div>
<h2>Konstruktive digitale Diskussionskultur</h2>

<p>
    Um die anstehenden Herausforderungen im Einklang mit der freiheitlich demokratischen Grundordnung bewältigen zu können, ist es unerlässlich, dass auch kontroverse Themen sachlich und lösungs&shy;orientiert diskutiert werden können. Aktuell ist leider zu beobachten, dass kontroverse Diskurse sehr oft ergebnislos abbrechen – entweder weil die Diskussion eskaliert, oder eine Seite die Kommunikation einstellt.
</p>
<p>
    Die Inititative zur Förderung einer konstruktiven digitalen Diskussionskultur (KDDK) hat zum Ziel, dieses Problem mit dem Fokus auf den digitalen Kommunikationsraum zu entschärfen.
</p>

<hr>

**Projekt Selbstverpflichtung**

Auch unter Menschen, die grundsätzlich ähnliche Ziele haben, verlaufen Diskussionen im digitalen Raum oft unkonstruktiv oder sie eskalieren sogar. Die fruchtbaren Beiträge gehen im Rauschen unter. Das kostet Ressourcen, die eigentlich dringend für die konstruktive Arbeit gebraucht werden.

Rauschen und unkonstruktive Beiträge werden sich kaum vollständig abstellen lassen. Ziel dieses Projekts ist es stattdessen, auf Microblogging-Netzwerken eine bessere Trennung zwischen gehaltvollen und unkonstruktiven Beiträgen, zwischen Signal und Rauschen, zu erreichen.

[Mehr Info](./selbstverpflichtung)

<hr>

**Salon digitale Diskussionskultur** (Veranstaltungsreihe 2024):


Im Rahmen dieser Reihe organisieren wir regelmäßig Onlineveranstaltungen, zu denen alle Interessierten beitragen können ([mehr Info](./salon)).

**Bisherige Termine:**

- 06\. Mai 2024, 19:00 Uhr: ***Werkstatt digitale Diskussionskultur***: ([mehr Info](./salon/2024-05-06.html))
- 14\. April 2024, 14:00 Uhr: **Innovative Tools for Debate Online: Insights from vTaiwan** ([mehr Info](./salon/2024-04-14.html))
- 04\. März 2024, 19:00 Uhr: ***Gemeinsam sind wir schlau: Was Mündigkeit mit Teamwork zu tun hat*** ([mehr Info](./salon/2024-03-04.html))
- 05\. Februar 2024, 19:00 Uhr: ***Alles muss man selber machen: Ein Vorschlag zur konstruktiven digitalen Diskussionskultur*** ([mehr Info](./salon/2024-02-05.html))

**Interesse oder Fragen?** [Melde dich bei uns!](#Kontakt).

<hr>

<div>
   <p><b>2023-12-28:</b> Vortrag: Warum sind digitale Diskussionen so frustrierend und wie können sie besser werden? (FireShonks23):</p>
     <ul>
        <li>
            <a href="https://pretalx.c3voc.de/fireshonks23/talk/ZKKHSD/">Fahrplan</a>
        </li>
        <li>
            <a href="https://media.ccc.de/v/37c3-57963-konstruktive-digitale-diskussionskultur-kddk#t=172">
            Video:<br><br>
            <img src="img/2023_fireshonks-talk-screenshot.jpg" width="80%">
            </a>
            <br>
            <br>
        </li>
        <li>
            <a href="https://hedgedoc.c3d2.de/p/KwqPns0Py#/">
            Folien
            </a>
        </li>
    </ul>

<hr>
   <p><b>2023-11-28:</b> Positionspapier</p>
     <ul>
        <li>
            <a href="positionspapier.html">12 Thesen zur Gründung der Initiative zur Förderung einer konstruktiven digitalen Diskussionskultur</a>
        </li>
    </ul>
<hr>
   <p><b>2023-09-16:</b> Workshop auf den Datenspuren</p>
     <ul>
        <li>
            Folien: <a href="https://hedgedoc.c3d2.de/p/9nFRp-fw0#/">https://hedgedoc.c3d2.de/p/9nFRp-fw0#/</a>
        </li>
        <li>
            Notizen aus dem Workshop (noch unbearbeitet): <a href="https://hedgedoc.c3d2.de/dWc40g9yQc-tqp8NvZMX7A?view">https://hedgedoc.c3d2.de/dWc40g9yQc-tqp8NvZMX7A?view</a>
        </li>
    </ul>
</div>

<hr>
<br>
<div>
    <p id="Kontakt">Kontakt:</p>
    <ul>
        <li>
            Mastodon: <a href="https://mastodontech.de/@kddk" rel="me">https://mastodontech.de/@kddk</a>
        </li>
        <li>
            Mail: kddk {ät} uber.space
        </li>
        <li>Matrix: <a href="https://matrix.to/#/#kddk:matrix.org">https://matrix.to/#/#kddk:matrix.org</a></li>
        <li>Signal: <a href="https://signal.group/#CjQKINNxipM4M31gGq6YW3TbDn67_U-qcQrMm1tIeAhmJtGEEhCzAOycIwatirqmVGrFmuCo">https://signal.group/#CjQKINNxipM4M31gGq6YW3TbDn67_U-qcQrMm1tIeAhmJtGEEhCzAOycIwatirqmVGrFmuCo</a></li>
        <li>Telegram: <a href="https://t.me/+DxXEmlgu5Xw5NTAy">https://t.me/+DxXEmlgu5Xw5NTAy</a></li>
    </ul>
</div>
