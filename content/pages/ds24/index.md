Title: Datenspuren 2024 Vortrag
Date: 2024-09-21
Author: kddk
save_as: ds24/index.html
Template: page
og_description: Material zum Vortrag: Diskussionskultur fixen! – Ein Bottom-Up-Opt-In-Lösungsvorschlag
image: /img/2024-09-21_ds24-folien-screenshot.jpg



# Diskussionskultur fixen!
## Ein Bottom-Up-Opt-In-Lösungsvorschlag

Am 21.09.2024 fand unter diesem Titel ein Vortrag mit anschließender Diskussion auf den [Datenspuren](https://datenspuren.de/) (jährliches Symposium des Chaos Computer Clubs Dresden) statt. Hier gibt es das Material zum Vortrag.

<img src="/img/2024-09-21_ds24-folien-screenshot.jpg" width="100%">


 <ul>
        <li>
            Video:<br><br>
            <a href="https://media.ccc.de/v/ds24-408-diskussionskultur-fixen-ein-bottom-up-opt-in-lsungsvorschlag">
            <img src="/img/2024-09-21_ds24_video_screenshot.jpg" width="60%">
            </a>
            <br>
            <a href="https://media.ccc.de/v/ds24-408-diskussionskultur-fixen-ein-bottom-up-opt-in-lsungsvorschlag">media.ccc.de</a>
            oder
            <a href="https://www.youtube.com/watch?v=bN7sBx-2fik">youtube</a><br><br>
        </li>
        <li>
            <a href="https://hedgedoc.c3d2.de/p/caCaePtsJ#/">
            Vortragsfolien
            </a>
            <br><br>
        </li>
        <li>
            <a href="/selbstverpflichtung.html">
            Seite des Selbstverpflichtungsprojektes (Basis des Vortrages)
            </a>
            <br><br>
        </li>
        <li>
            <a href="https://talks.datenspuren.de/ds24/talk/YCNET8/">Fahrplan</a> (ursprüngliche Ankündigung)
        </li>
</ul>


